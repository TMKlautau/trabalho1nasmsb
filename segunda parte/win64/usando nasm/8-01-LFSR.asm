; Autor Tito Klautau
; Para compila utilise 'nasm -f elf LFSR.o'

%macro xnorSelectedBits 0 ;polinomio usado retirado de http://www.xilinx.com/support/documentation/application_notes/xapp052.pdf

	mov eax, dword[currentState];carrega o numero atual em eax
	mov ebx, eax				;copia eax para ebx para manter o bit que queremos em eax
	shr ebx, 1					;seta a tap 23
	xor eax, ebx				;xnor 24, 23
	not eax						
	shr ebx, 1					;seta a tap 22
	xor eax, ebx				;xnor 24, 23, 22
	not eax
	shr ebx, 5					;seta a tap 17
	xor eax, ebx				;xnor 24, 23, 22, 17
	not eax
	shr dword[currentState], 1	;prepara o bit oposto no numero atual para receber o bit correto
	shl eax, 23					;coloca o bit correto na posição certa
	and eax, 00ffffffh			;limpa o lixo em eax
	or  dword[currentState], eax;coloca o bit no numero atual
	
%endmacro

segment .data

segment .bss

currentState resd 1

interArray resd 4096

segment .text

	global _generateTextLSFR

	_generateTextLSFR:	
		push ebp
		mov ebp, esp
		
		%assign i 0
		
		%rep 4096
			
			mov dword[interArray+i], 0	;seta o array inteiro dos intervalos em 0
			%assign i i+1
			
		%endrep
		
		mov eax, [ebp+12] ;carrega o segundo parametro em eax, este é o estado inicial
		
		mov dword[currentState], eax ;carrega o estado inicial no numero atual
		
		mov ecx, [ebp+8] ; carrega a quantidade de numeros a serem gerados em ecx
		
		startloop:
		
			xnorSelectedBits ;efetua o LFSR
			mov eax, dword[currentState] 
			shr eax, 12 ;divide o numero atual por 4096 para saber em que intervalo se encontra
			add dword[interArray + (eax*4)], 1 ;incrementa o intervalo correspondente no array
			sub ecx, 1 ;decrementa ecx
			cmp ecx, 0 ;compara com 0
			
		jne startloop ;verifica se o loop continua ou para
		
		
		mov eax, interArray ;retorna o endereço para o array
		
		mov esp, ebp
		pop ebp
		ret