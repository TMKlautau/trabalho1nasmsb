/* Autor: Tito Klautau
 * Para compilar utilise 'gcc -o "nome do executavel q vc quiser" secondpart.c LFSR.o -std=c99
 * Aceita ser chamado com os seguintes parametros, qtd_a_ser_gerada, seed
 * seed deve ser menor q 16777215
 */
#include <stdio.h>
#include <math.h>

int* generateTextLSFR(int qtd_a_ser_gerada, int seed); // função do nasm que recebe a quantidade de numeros a ser gerada e a seed, esta tem que ser menor que 16777215 pois este é o deadlock do LFSR e tem que ter no max 24 bits

int main(int argc, char **argv){
			
	double expectedVariance, xSquared = 0, qtd_a_ser_gerada = 16777215;
	
	int seed = 0;
	
	FILE *relatorio;

	if(argc == 3){
		sscanf(argv[1],"%lf",&qtd_a_ser_gerada);
		sscanf(argv[2], "%d",&seed);
	}

	int* interArray = generateTextLSFR(qtd_a_ser_gerada, seed);	// gera o array contendo a quantidade de numeros gerados em cada intervalo
	
	relatorio = fopen("relatorioLSFRNasm.txt","w");
	
	expectedVariance = qtd_a_ser_gerada/4096; // calcula uma expectedVariance prox da atual se todos os numeros tem a mesma probabilidade de aparecer
	
	fprintf(relatorio, "quantidade de numeros gerados: %0.f\n", qtd_a_ser_gerada);
	
	for(int i = 0; i<4096; i++){

		fprintf(relatorio,"o grupo %d tem %d elementos, o esperado era de %f \n",i,interArray[i], expectedVariance);

		xSquared += (pow((interArray[i] - expectedVariance),2))/expectedVariance; //calcula o chi-squared
	}

	fprintf(relatorio,"ChiSquared = %f\n",xSquared);
	fclose(relatorio);
	
	printf("relatorio gerado");
	
	return 0;
	
}