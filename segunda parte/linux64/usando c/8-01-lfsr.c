#include <stdio.h>
#include <stdlib.h>
#include <math.h>

uint32_t lfsr_return(uint32_t estado_atual);

int main(int argc, char **argv){
    FILE *fp;
    uint32_t *estatistica, estado = 8725643; //a semente e inicializada
    float chi_quad = 0;
    
    fp = fopen("relatorio.txt","w"); //arquivo de relatorio
    
    estatistica = (uint32_t*) calloc (4096, sizeof(uint32_t)); //uma tabela com as estatisticas inicialmente zeradas
    
    for (int i = 0; i < 4096; i++){
        for(int j = 0; j < 4096; j++){
            estado = lfsr_return(estado); //realiza o lfsr
            estatistica[estado%4096]++; //adiciona a estatistica
        }
    }
    
    //imprimir o relatorio
    fprintf(fp, "Semente: %u.\n", estado);
    
    for(int j = 0; j < 4096; j++){
        fprintf(fp, "Frequencia de %d - esperado: %d - %d.\n", j, estatistica[j], 4096);
        chi_quad += pow(((float)estatistica[j]-4096),2)/4096;
    }
    fprintf(fp, "Chi-Quadrado: %f.\n", chi_quad);
    
    free(estatistica);  //liberar ponteiro
    fclose(fp);         //fechar arquivo
    
    return 0;
}

uint32_t lfsr_return(uint32_t estado_atual){
    uint32_t bit_retorno;
    bit_retorno = ( (estado_atual) ^ (estado_atual >> 1) ^ (estado_atual >> 2) ^ (estado_atual >> 7) ) & 1; // nor com bits (24,23,22 e 17)
    return ( (estado_atual >> 1) | (bit_retorno << 23) );   //retorna o bit modificado
}