/*
 * Para compilar use "gcc -o 'nome do executavel' firstpartlinux.c  conditions.o -std=c99 -m32"

 */

#include <stdio.h>

// do-while, while-do, for, switch e if-else

// functions
int _testdowhile(int a, int b);

int _testwhile(int a, int b);

int _testfor();

int _testswitchcase(int a);

int _testifelse(int a, int b);

// main
int main()
{
	
	printf("%d\n", _testdowhile(0,1));

	printf("%d\n", _testwhile(0,1));

	printf("%d\n", _testfor());

	printf("%d\n", _testswitchcase(3));

	printf("%d\n", _testifelse(2,1));
	
	return 0;
}
