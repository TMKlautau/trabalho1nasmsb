/*
 * Para compilar use 'gcc -o "nome do executavel" conditions.o firstpart.c'
 */

#include <stdio.h>

int testdowhile(int a, int b);

int testwhile(int a, int b);

int ifelse(int a, int b);

int switchcase(int a);

int testfor();

int main(){
	
	printf("%d\n", ifelse(2,1));
	
	printf("%d\n", switchcase(3));
	
	printf("%d\n", testfor());
	
	printf("%d\n", testwhile(0,1));
	
	printf("%d\n", testdowhile(0,1));

	return 0;
}