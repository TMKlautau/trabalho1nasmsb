; Autor: Tito Klautau
; para compilar use 'nasm -f elf conditions.asm'

%macro mDO 0

	%push cdo
	%$startloop:

%endmacro

%macro mENDDOWHILE 3 ;recebe 3 parametros, 1 e 3 são os endereços das variaveis a se testar, o segundo é o condition code

	%ifctx cdo
		push eax
		mov eax, [%1]
		cmp eax, [%3] ;compara os 2 operandos para setar as flags
		pop eax
		j%-2 %$startloop ;efetua o jump correspondente ao inverso do CC colocado no primeiro argumento
		%pop cdo
	%else
		%error "eh necessario chamar mDO antes de mENDDOWHILE"
	%endif
	
%endmacro

%macro mWHILE 3 ;recebe 3 parametros, 1 e 3 são os endereços das variaveis a se testar, o segundo é o condition code

	%push cwhile ;cria o contexto cwhile
	%$startloop:
	push eax
	mov eax, [%1]
	cmp eax, [%3] ;compara os 2 operandos para setar as flags
	pop eax
	j%+2 %$notmwhile ;efetua o jump correspondente ao CC colocado no primeiro argumento
	
%endmacro

%macro mENDWHILE 0
	
	%ifctx cwhile ;verifica se o contexto é cwhile
		jmp %$startloop
		%$notmwhile: ;seta o final do mwhile para pular caso não passe pelo while
		%pop cwhile ;retira o contexto cwhile da pilha, se o cwhile não for o do topo gera erro
	%else
		%error "eh necessario chamar mWHILE antes de mENDWHILE"
	%endif

%endmacro

%macro mFOR 3 ;necessita de 3 parametros, 

	%push cfor
	jmp %$startloop
	%$var: dd %1 ;o primeiro é um ponteiro para a variavel que será usada no for, usei ponteiro pois não consegui inicializar uma variavel no .text
	%$lim: dd %2 ;o segundo eh o limite que a variavel alcança, quando ela chegar ao limite ou passa-lo o for para
	%$direc: dd %3 ;o terceiro eh o quanto a variavel eh incrementada em cada iteracao
	%$startloop:

%endmacro

%macro mENDFOR 0
	
	%ifctx cfor
		test dword[%$direc], 1
		js %%negative
		
		push eax
		mov eax, [%$var]
		mov eax, [eax]
		cmp eax, [%$lim]
		jae %%endfor	
		add eax, [%$direc]
		push ebx
		mov ebx, [%$var]
		mov dword[ebx], eax	
		pop ebx
		pop eax
		jmp %$startloop
		
		%%negative:
		push eax
		mov eax, [%$var]
		mov eax, [eax]
		cmp eax, [%$lim]
		jbe %%endfor	
		add eax, [%$direc]
		push ebx
		mov ebx, [%$var]
		mov dword[ebx], eax	
		pop ebx
		pop eax
		jmp %$startloop
		
		%%endfor:
		pop eax
		%pop cfor
	%else
		%error "eh necessario chamar mFOR antes de mENDFOR"
	%endif
	
%endmacro


%macro mSWITCH 1 ;chama o switch, recebe a variavel que será testada nos cases
	
	%push cswitch ;coloca o contexto cswitch na pilha
	
	%define %$condition %1	; define o macro local a cswitch da condição passada como o parametro
	
%endmacro

%macro mCASE 2 ;chama o case, recebe o CC para testar e o valor que deve ser a condição para este case

	%ifctx cswitch ; verifica se o case está num switch
		%push ccase ; cria um novo contexto para o case
		
		push eax 
		mov eax, %2
		cmp eax, %$$condition ;compara para verificar se executa o case
		pop eax
		j%-1 %$notcase ;pula para o final do case caso não for para executar
			
	%else
		%error "eh necessario chamar mSWITCH antes de mENDSWITCH"
	%endif
	
%endmacro

%macro mENDCASE 0 ;termina o case, criei o endcase e o break pois em c++ é possivel uma variavel cair em 2 cases de um switch, neste caso como somente testa para igualdade ambos funcionam parecidos, soh que o break não verifica os cases seguintes

	%ifctx ccase ;verifica se vem seguido de um case
		%$notcase: 
		%pop ccase ;retira o contexto ccase
	%else
		%error "eh necessario chamar mCASE antes de mENDCASE"
	%endif
	
%endmacro

%macro mBREAK 0 ;termina o switch caso o case anterior tenha sido rodado

	%ifctx ccase
		jmp %$$lendswitch ; efetua um jump para a label lendswitch do contexto cswitch que se encontra na segunda posição da pilha
		%$notcase:
		%pop ccase	
	%else
		%error "eh necessario chamar mCASE antes de mBREAK"
	%endif

%endmacro

%macro mENDSWITCH 0 ;desempilha os contextos, no caso de nao haver um break, somente o cswitch, se houve break, o ccase e o cswitch

	%ifctx cswitch
		%$lendswitch:
		%pop cswitch
	%elifctx ccase
		%$lendswitch:
		%pop ccase
		%pop cswitch
	%else
		%error "eh necessario chamar mSWITCH antes de mENDSWITCH"
	%endif
	
%endmacro

%macro mIF 3 ;macro utilizado para verificar igualdade entre os parametros

	%push cif ;cria o contexto mif, que será usado nas macros else e mif
	push eax
	mov eax, %1
	cmp eax, %3 ;compara os 2 operandos para setar as flags
	pop eax
	j%-2 %$notmIF ;efetua o jump correspondente ao inverso do CC colocado no primeiro argumento
	
%endmacro

%macro mELSE 0

	%ifctx cif;verifica se o else vem depois de um if
		%repl celse ;renomeia o contexto para melse
		jmp %$notmELSE ;se o if foi tomado pula o else
		%$notmIF:	;se o if não foi tomado efetua o else	
	%else
		%error "eh necessario chamar mIF antes de mELSE" ;mensagem de erro
	%endif

%endmacro

%macro mENDIF 0 ;macro utilizado para terminar o if
	
	%ifctx cif ;verifica se o contexto é mif
		%$notmIF: ;seta o final do mIF para pular caso não passe pelo if
		%pop cif ;retira o contexto mif da pilha, se o mif não for o do topo gera erro
	%elifctx celse ;verifica se o contexto é melse
		%$notmELSE: ;seta o final do melse
		%pop celse ;retira o contexto melse da pilha, se o mif não for o do topo gera erro
	%else
		%error "eh necessario chamar mIF antes de mENDIF" ;mensagem de erro
	%endif

%endmacro



segment .data

segment .bss

auxfor: resd 1

segment .text

	global _testdowhile
	
	global _testwhile

	global _testfor

	global _switchcase
	
	global _ifelse
	
	_testdowhile:
		push ebp
		mov ebp, esp
		
		xor eax, eax
	
		mDO
		
		add eax, 4
		add dword[ebp+8], 1
		
		mENDDOWHILE ebp+8, e, ebp+12 
		
		mov esp,ebp
		pop ebp
		ret	

	
	_testwhile:
		push ebp
		mov ebp, esp
		
		xor eax, eax
	
		mWHILE ebp+8, e, ebp+12 
		
		add eax, 4
		add dword[ebp+8], 1
		
		mENDWHILE
		
		mov esp,ebp
		pop ebp
		ret
	
	_testfor:
		push ebp
		mov ebp, esp
		
		xor eax, eax
		
		mov dword[auxfor], 0		
		mFOR auxfor, 10, 1 ; for(int auxfor; auxfor <= 10; auxfor+=1)
		
		add eax, 4
		
		mENDFOR
		
		mov esp,ebp
		pop ebp
		ret

	_switchcase:
		push ebp
		mov ebp, esp
		
		mSWITCH [ebp+8]
		
		mCASE e, 1
		mov eax, 10
		mENDCASE
		mCASE e, 2
		mov eax, 20
		mBREAK
		mCASE e, 3
		mov eax, 30
		mENDCASE
		mENDSWITCH
		
		
		mov esp,ebp
		pop ebp
		ret

	_ifelse:
		push ebp
		mov ebp,esp
		
		mIF [ebp+8], ne, [ebp+12]
		mov eax, 0
		mELSE
		mov eax, 1
		mENDIF
	
		mov esp,ebp
		pop ebp
		ret